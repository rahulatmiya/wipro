package ActionClass;



import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Com.Framework.BaseClass;

public class Action extends BaseClass {
	 String url = "";
     HttpURLConnection request = null;
    int response = 0;
	
	public void TotalHyperLink() {
		List<WebElement> links = driver.findElements(By.tagName("a"));
		System.out.println("Total numbers of hyperlinks :"+links.size());  
		
	}
	  									
	public void validateBrokenlink() {
		List<WebElement> links = driver.findElements(By.tagName("a"));
		Iterator<WebElement> list=links.iterator();
		while(list.hasNext()){
			url=list.next().getAttribute("href");
		
		if(url!=null) {
//			System.out.println(url);
			try {
				request = (HttpURLConnection)(new URL(url).openConnection());
				 request.setRequestMethod("GET");
		         request.connect();
		         response = request.getResponseCode();
		         
		         if(response!=200)
		        	 System.out.println(url+"---This is the Broken link and their response is:"+response);
			} catch (MalformedURLException e) {
			e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		         
	}
		}
	}
}
