package ObjectClass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import ActionClass.Action;



public class Pageobject {

	@FindBy(xpath="//input[@title='Search']")
	public static WebElement Search;
	

static {

	PageFactory.initElements(Action.driver, Pageobject.class);
}
}