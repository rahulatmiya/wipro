package Com.Framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Setup {
	public static WebDriver driver;
	
	@BeforeTest
	public static void initial() {
		String path=System.getProperty("user.dir");
		System.out.println(path);
		System.setProperty("webdriver.chrome.driver",path+"/src/Resources/chromedriver.exe");
		
        driver=new ChromeDriver();
        driver.get("https://www.google.com/");
        driver.manage().window().maximize();
	}
	
	@AfterTest
	public static void End() throws InterruptedException {
		
		Thread.sleep(10000);
		driver.quit();
	}
}
