package Com.Framework;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class BaseClass extends Setup {

	public  void Sendkeys(WebElement element,String Data) {
		try{
			element.sendKeys(Data);
			element.submit();
		
		}
		catch(NoSuchElementException e)
		{
			e.printStackTrace();
		}
		}
}
